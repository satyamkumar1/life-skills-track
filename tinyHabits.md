# Tiny Habits

### Question 1: Your takeaways from the video (Minimum 5 points)

- "Tiny habits" is a powerful technique for behavior change through small adjustments. If I am doing something then I should celebrate my victory.
- After I do this I will do that, this is the best way to set a tiny habit.
- Planting the right seed at the right time lets us grow naturally.


### Question 2: Your takeaways from the video in as much detail as possible?

- If I want to develop a new habit then I have to shrink it to the tinest version and do it.
- Attach a trigger to perform the habit using After I do this I will do that.
- Celebrate small wins when a particular tiny habit is practiced.


### Question 3: How can you use B = MAP to make making new habits easier?

- B=MAP stands for, motivation, ability, prompts

- Motivation: Discovering a strong reason why I want to develop the desired - habit. Then using that reason as motivation.
- Ability: Breaking down the habit into manageable steps that are easier to accomplish. This increases the ability to perform the task.
- Prompt: Establish reminders that prompt me to perform the habit.


### Question 4: Why it is important to "Shine" or Celebrate after each successful completion of a habit?

- It's important to celebrate each time after I successfully complete a habit because this celebration will reinforce my positive behavior and keeps me motivated to keep going.


### Question 5: Your takeaways from the video (Minimum 5 points)

- The talk is about improving ourselves in different areas of life, like health, productivity, and relationships.
- By establishing small daily rituals, we can build momentum and achieve long-term success.
- The focus is on having a positive mindset and enjoying the journey of self-improvement.


### Question 6: Write about the book's perspective on habit formation from the lens of Identity, processes, and outcomes.

- Identity: The book emphasizes the importance of seeing ourselves as the type of person who engages in desired habits.
- Processes: It suggests focusing on small, consistent actions and developing effective systems to build habits.
- outcomes: focus on building daily habits and the actions that lead to desired outcomes, rather than just obsessing over end results.


### Question 7:Write about the book's perspective on how to make a good habit easier.

- simplifying good habits by breaking them down into smaller, manageable steps and emphasizing consistency in their practice.


### Question 8: Write about the book's perspective on making a bad habit more difficult.

- In "Atomic Habits," the book suggests making bad habits less appealing and convenient by increasing friction and introducing obstacles.


### Question 9: Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

- I would pick a habit of reading the newspaper.
- Initially, I can start with headings, as it is easier and will motivate me to read the full article if the heading attracts my interests such as politics and sports.
- I will make a trigger for it using After I _____ I will ____(read newspaper), this will surely help me to read the newspaper regularly.

### Question 10: Pick one habit that you would like to eliminate or do less of. What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

- I would like to eliminate Instagram scrolling.
- The best way will be to make my feed more boring by unfollowing meme pages.
- Using a website blocker on Chrome and an app blocker on mobiles will surely increase the friction of frequently scrolling Instagram.