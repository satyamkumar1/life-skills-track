# Life Skills Track

## 1. How to Learn Faster with the Feynman Technique

### Question 1: What is the Feynman Technique? Paraphrase the video in your own words.
- Don't fool your self, you are the easiest person to fool.

- so the best way to understand a concept is to teach the concept to someone in layman's terms.

- Feynman is a learning technique that suggests that to make sure you understand the concept completely and be ready to explain it to others.
- even if the other person has no basic knowledge about it.
- Learn the concepts in such a way that if anybody asks why it works, you should be able to explain why this thing holds true and works.


### Question 2. What are the different ways to implement this technique in your learning process?

The different ways to implement this technique are-

- Take a paper and write the heading of the concept which you are learning.
- Explain all the concepts in simple plain English or any other language such that the person who read understands it.
- If getting stuck anywhere between refer to the source and clear it.
- If there are any complex technological terms break it down into simpler words.


## 2. Learning How to Learn TED talk by Barbara Oakley

### Question 3: Paraphrase the video in detail in your own words.

- The video tells us about how to learn anything. There are two states of mind focus state and relaxed state.
- To learn efficiently we need to keep switching between these two states.
- A slow learning process can bring in-depth knowlegde of the source material.
- Trying to recall after seeing a page of notes can help you make new neural connections and have a better memory over it.
- When we switch between two states it helps us to find a more creative approach to solve a problem in a relaxed state.

- The Speaker stated the example of Thomas Edison, he use to keep ball bearings in his hand, and when in a relaxed state he is almost about to sleep bearings fall and he woke up, then he applies the ideas from his relaxed state to a focused state.


### Question 4: What are some of the steps that you can take to improve your learning process?

- The one problem in the method that arises is procrastinating, to solve this problem Pandora approach can be used.
- In the Pandora approach, we can set the timer to 25 - 30 minutes and work with full focus during that duration by avoiding all distractions.
- Then after that, we can take a break for 4 - 5 minutes and again repeat the process.


## 3. Learn Anything in 20 hours

### Question 5: Your key takeaways from the video? Paraphrase your understanding.


- Anything can be learned in 20 hours and to become really good at something requires a lot of practice and 10 thousand hours of practice can make us reach the top of our field.


- The barrier to learning anything new is an emotional barrier which can be removed by not worrying about others, obviously, it feels dumb when we start learning anything new but 20 hours of focused learning can make us good at it.



### Question 6: What are some of the steps that you can while approaching a new topic?

- A Skill looks huge when looked at from the outside, so to learn I need to break it into smaller things and started learning them.
- Practice it, again and again, to become really good at it.