# Grit and Growth Mindset

### Question 1: Paraphrase (summarize) the video in a few lines. Use your own words.

- A high IQ does not guarantee success in life, what matters is having "grit," which means having a growth mindset that keeps us motivated to keep performing.
- It is ok to fail but learn from the failures and improve further in life.


### Question 2: What are your key takeaways from the video to take action on?

- Having a growth mindset which helps in keeping performing.
- The ability to learn can improve by putting in a little more effort.


### Question 3: Paraphrase (summarize) the video in a few lines in your own words.

- There are two mindsets: the fixed mindset and the growth mindset.
- In a fixed mindset, people believe their abilities are fixed and cannot be improved.
- A fixed mindset leads to avoidance of challenges and feedback.
- A growth mindset believes abilities can be developed through effort.
- People with a growth mindset embrace challenges and see feedback as an opportunity to learn and grow.


### Question 4: What are your key takeaways from the video to take action on?

- Growth Mindset: Believing in the potential for skill development and improvement over time.
- Key Ingredients for Growth: Effort, challenges, mistakes, and feedback as crucial elements for personal growth and learning.


### Question 5: What is the Internal Locus of Control? What is the key point in the video?

- Internal Locus of Control: Belief in personal control over life and outcomes.
- Importance: Motivation and responsibility stem from an internal locus of control.
- Study Findings: Internal locus of control linked to higher motivation and perseverance.
- Benefits: Empowerment, motivation, and belief in personal effort as a driver of success.


### Question 6: Paraphrase (summarize) the video in a few lines in your own words.

- Growth Mindset: Belief in your ability to learn and improve.
- Question Assumptions: Challenge existing beliefs and limitations.
- Future Potential: Don't let current abilities limit future possibilities.


### Question 7: What are your key takeaways from the video to take action on?

- Question and Grow: Challenge assumptions and foster personal growth.
- Take Control: Design your own learning path for continuous improvement.
- Embrace Challenges: See obstacles as opportunities to grow.
- Build Resilience: Stay positive and persevere through difficulties.
- Learn from Feedback: Value criticism as a pathway to learning.


### Question 8: What are one or more points that you want to take action on from the manual? (Maximum 3)

- I will treat new concepts and situations as learning opportunities. I will never get pressurized by any concept or situation.
- I will understand each concept properly.
- give time to learn new things don't be afrid