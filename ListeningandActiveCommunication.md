# Active Listening

### Question 1: What are the steps/strategies to do Active Listening?

- Active listening is hearing and completely comprehending the meaning of the contents said by the speaker.
- Focus on the listeners’ words and don't interrupt in between.
- Keep the points when the other person is finished.
- Phrases such as " Tell me more", and "That sounds interesting" shows - interest in the speaker's content and let the other people talk more.
- Body language is important, show interest by making eye contact with the speaker.
- Take notes during an important conversation.
- Paraphrase the summary of information to make sure the speaker and you are on the same page.


### Question 2: According to Fisher's model, what are the key points of Reflective Listening?

- Reflective listening is a way of communication where it talks about listening to the speaker's idea and stating it back to the speaker, to confirm that it has been understood.
- It is not just speaking back words which the speaker has said but also saying them back with genuine understanding.
- It can be helpful while talking to someone who is depressed by making him feel that he is being listened to.


### Question 3: What are the obstacles in your listening process?

- Distraction, Thinking about something else even for a few seconds makes me lose the context and have to ask to repeat once again.
- Zoning out when the conversation is boring.
- Sometimes not able to connect with the actual feelings of the speaker.


### Question 4: What can you do to improve your listening?

- Using phrases such as "Tell me more" will help me not zone out and listen to the person.
- Asking questions related to his content will keep me involved in the conversation, so I will not feel bored.
- I will keep my body language good, such as maintaining eye contact, nodding my head, and smiling at the appropriate place will help me to connect emotionally.


### Question 5: When do you switch to a Passive communication style in your day-to-day life?

- When something is bothering me about a person but that person is my close friend or maybe in the future he will be useful regarding some work.
- When I am not sure I am right or not.
- When the other person has more knowledge than me.


### Question 6: When do you switch to Aggressive communication styles in your day-to-day life?

- When somebody is not listening to me in spite of making several requests.
- When I am not in the mood to do the work


### Question 7: When do you switch to Passive Aggressive communication styles in your day-to-day life?

- Honestly, I don't think I ever have passive-aggressive communication because i don't gossip about others or talk bad about others.


### Question 8: How can you make your communication assertive?

- We can make our communication by clearly communicating our needs.
- It may get rejected but it is better to speak up rather than quietly fulfill other needs over ours.
- we don't have to apologize for our needs as we are humans and humans have needs.
- when we are open with our needs it helps other people also to come up with their needs.