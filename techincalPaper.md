# Project Title

 ### General Mvc Architecture And Spring Framework

 # Introduction

Spring MVC is a Java framework that is used to develop web applications. It is built on a Model-View-Controller (MVC) pattern and possesses all the basic features of a spring framework, such as Dependency Injection, Inversion of Control. 



## Importance Of  MVC Architecture

- The Model-View-Controller (MVC) framework is an architectural/design pattern that separates an application into three main logical components Model, View, and Controller. 

### Components of MVC :

The MVC framework includes the following 3 components:

1. Controller
2. Model
3. View

### 1.Controller:

- The controller is the component that enables the interconnection between the views and the model so it acts as an intermediary. 
- The controller doesn’t have to worry about handling data logic.
 - it just tells the model what to do. It process all the business logic and incoming requests

### 2.View:

- The View component is used for all the UI logic of the application.
-  It generates a user interface for the user.
-  Views are created by the data which is collected by the model component but these data aren’t taken directly but through the controller. 
- It only interacts with the controller.

### 3.Model:

- The Model component corresponds to all the data-related logic that the user works with. 
- This can represent either the data that is being transferred between the View and Controller components or any other business logic-related data.
-  It can add or retrieve data from the database.
- It responds to the controller’s request because the controller can’t interact with the database by itself.
- The model interacts with the database and gives the required data back to the controller.


### Advantages of MVC:

- Codes are easy to maintain and they can be extended easily.
- The MVC model component can be tested separately.
- The components of MVC can be developed simultaneously.
- It reduces complexity by dividing an application into three units. Model, view, and controller.
- It supports Test Driven Development (TDD).

 ### Disadvantages of MVC:

- It is difficult to read, change, test, and reuse this model
- It is not suitable for building small applications.
- The inefficiency of data access in view.
- The framework navigation can be complex as it introduces new layers of abstraction .

### Popular MVC Frameworks:

- Ruby on Rails
- Django
- CherryPy
- Spring MVC

## Spring Framework

Spring framework is flexible as it supports and can be integrated with various technologies like:

- It supports the REST style of web services.
- Supports transactional management
- The developer can interact with different databases.
- Can be integrated with Object Relationship frameworks, for instance, iBatis
- Containers can resolve the required dependencies as it supports Dependency Integration.

## Examples of how you, as an application developer, can use the Spring platform advantage:

- Make a Java method execute in a database transaction without having to deal with transaction APIs.

- Make a local Java method a remote procedure without having to deal with remote APIs.

- Make a local Java method a management operation without having to deal with JMX APIs.

- Make a local Java method a message handler without having to deal with JMS APIs.