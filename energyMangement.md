# Energy Management

### Question 1: What are the activities you do that make you relax - Calm quadrant?

- Listening to music.
- Watching video on youtube.
- Coffee
- watching comdey.


### Question 2: When do you find getting into the Stress quadrant?

- When I am crossing my deadline.
- When I do not perform well in reviews or in any task.
- sometime thinking about future.


### Question 3: How do you understand if you are in the Excitement quadrant?

- When I am performing well on the task.
- When I go prepared for any task in advance.
- when there is no work load.


### Question 4: Paraphrase the Sleep is your Superpower video in detail.

- Sleep is a time for my body and brain to rest and recharge.
- It helps me remember things better by organizing what I learned during the day.
- Getting enough sleep makes me feel happy, energized, and ready for the day.
- Sleep is important for my body's growth, healing, and staying healthy.
- It repairs damage, boosts my immune system, and helps me fight off sickness.
- Not getting enough sleep can make me grumpy, have trouble paying attention,  and forget things.
- Having a regular sleep routine and a calm sleep environment can help me sleep better.


### Question 5: What are some ideas that you can implement to sleep better?

- Keep a consistent sleep schedule by going to bed and waking up at the same time every day.
- Create a cozy sleep environment by making your bedroom cool, dark, and quiet.
- Avoid using electronic devices before bed as they can disrupt your sleep.
- Establish a relaxing bedtime routine, like reading or taking a warm bath.
- Stay physically active during the day to help you feel tired at night.


### Question 6: Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

- Increased blood flow to the brain, delivering oxygen and nutrients for brain health
- Release of chemicals that promote happiness and reduce stress
- Enhanced cognitive function, including memory and problem-solving skills
- Growth of new neurons and strengthening of brain cell connections
- Reduced risk of cognitive decline and brain diseases
- Better sleep quality for optimal brain function



### Question 7:What are some steps you can take to exercise more?

- Create a schedule for regular exercise sessions
- Choose activities that I enjoy
- Set realistic goals and track progress
- Incorporate physical activity into daily life
- Join fitness classes or groups
- Use technology tools for monitoring and motivation